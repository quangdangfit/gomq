package schema

type RoutingKeyQueryParam struct {
	Group string `json:"group,omitempty"`
	Value uint   `json:"value,omitempty"`
}
